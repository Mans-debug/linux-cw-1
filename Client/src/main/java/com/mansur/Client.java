package com.mansur;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static final int PORT = 1303;
    public static final String IP_REGEX = "^((25[0-5]|(2[0-4]|1\\d|[1-9]|)\\d)\\.?\\b){4}$";

    public static void main(String[] args) {
            System.out.print("IP of the server: ");
            Scanner sc = new Scanner(System.in);
            String serverIp = sc.next();
            System.out.println();
            if (!serverIp.matches(IP_REGEX)) {
                System.out.println("Incorrect IP address");
                return;
            }
            try (Socket socket = new Socket(serverIp, PORT);
                 Scanner scanner = new Scanner(socket.getInputStream())) {
                String curTime = scanner.nextLine();
                System.out.printf("Current time is %s\n", curTime);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}