package com.mansur;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;

import static java.time.format.DateTimeFormatter.ofPattern;

public class Server {

    public static final int PORT = 1303;

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            while (true) {
                try (Socket socket = serverSocket.accept();
                     OutputStream outputStream = socket.getOutputStream();
                     PrintWriter printWriter = new PrintWriter(outputStream)) {
                    String time = LocalDateTime.now().format(ofPattern("dd-MM-yyyy hh:mm:ss"));
                    printWriter.println(time);
                }
            }
        }
        catch (IOException ioException){
            ioException.printStackTrace();
        }
    }
}